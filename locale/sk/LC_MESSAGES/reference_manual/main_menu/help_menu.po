# translation of docs_krita_org_reference_manual___main_menu___help_menu.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___main_menu___help_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-04-02 12:10+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "About KDE"
msgstr "Informácie o KDE"

#: ../../reference_manual/main_menu/help_menu.rst:1
msgid "The help menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:11
#, fuzzy
#| msgid "About KDE"
msgid "About"
msgstr "Informácie o KDE"

#: ../../reference_manual/main_menu/help_menu.rst:11
#, fuzzy
#| msgid "Krita Handbook"
msgid "Handbook"
msgstr "Príručka Krita"

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Bug"
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:16
msgid "Help Menu"
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:18
msgid "Krita Handbook"
msgstr "Príručka Krita"

#: ../../reference_manual/main_menu/help_menu.rst:19
msgid "Opens a browser and sends you to the index of this manual."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:20
msgid "Report Bug"
msgstr "Nahlásiť chybu"

#: ../../reference_manual/main_menu/help_menu.rst:21
msgid "Sends you to the bugtracker."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:22
msgid "Show system information for bug reports."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:23
msgid ""
"This is a selection of all the difficult to figure out technical information "
"of your computer. This includes things like, which version of Krita you "
"have, which version your operating system is, and most prudently, what kind "
"of OpenGL functionality your computer is able to provide. The latter varies "
"a lot between computers and due that it is one of the most difficult things "
"to debug. Providing such information can help us figure out what is causing "
"a bug."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:24
msgid "About Krita"
msgstr "O Krita"

#: ../../reference_manual/main_menu/help_menu.rst:25
msgid "Shows you the credits."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:27
msgid "Tells you about the KDE community that Krita is part of."
msgstr ""
