# translation of docs_krita_org_reference_manual___dockers___pattern_docker.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___pattern_docker\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-04-02 09:50+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/dockers/pattern_docker.rst:1
msgid "Overview of the pattern docker."
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:11
msgid "Patterns"
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:16
msgid "Patterns Docker"
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:19
#, fuzzy
#| msgid ".. image:: images/en/Krita_Patterns_Docker.png"
msgid ".. image:: images/dockers/Krita_Patterns_Docker.png"
msgstr ".. image:: images/en/Krita_Patterns_Docker.png"

#: ../../reference_manual/dockers/pattern_docker.rst:20
msgid ""
"This docker allows you to select the global pattern. Using the open-file "
"button you can import patterns. Some common shortcuts are the following:"
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:22
msgid "|mouseright| a swatch will allow you to set tags."
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:23
msgid "|mouseleft| a swatch will allow you to set it as global pattern."
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:24
msgid ":kbd:`Ctrl + scroll` you can resize the swatch sizes."
msgstr ""
