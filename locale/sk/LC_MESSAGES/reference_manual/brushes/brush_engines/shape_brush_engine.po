# translation of docs_krita_org_reference_manual___brushes___brush_engines___shape_brush_engine.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___shape_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 08:45+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Hard Edge"
msgstr "Ostré hrany"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:1
msgid "The Shape Brush Engine manual page."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:16
msgid "Shape Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
#, fuzzy
#| msgid "Experiment Option"
msgid "Experiment Brush Engine"
msgstr "Experimentálna možnosť"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Al.Chemy"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:19
msgid ".. image:: images/icons/shapebrush.svg"
msgstr ".. image:: images/icons/shapebrush.svg"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:20
msgid "An Al.chemy inspired brush-engine. Good for making chaos with!"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:23
msgid "Parameters"
msgstr "Parametre"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:25
msgid ":ref:`option_experiment`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:26
msgid ":ref:`blending_modes`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:31
msgid "Experiment Option"
msgstr "Experimentálna možnosť"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:33
msgid "Speed"
msgstr "Rýchlosť"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:34
msgid ""
"This makes the outputted contour jaggy. The higher the speed, the jaggier."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:35
msgid "Smooth"
msgstr "Plynulý"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:36
msgid ""
"Smoothens the output contour. This slows down the brush, but the higher the "
"smooth, the smoother the contour."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:37
msgid "Displace"
msgstr "Posunúť"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:38
msgid ""
"This displaces the shape. The slow the movement, the higher the displacement "
"and expansion. Fast movements shrink the shape."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:39
msgid "Winding Fill"
msgstr "Veterná výplň"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:40
msgid ""
"This gives you the option to use a 'non-zero' fill rules instead of the "
"'even-odd' fill rule, which means that where normally crossing into the "
"shape created transparent areas, it now will not."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:42
msgid "Removes the anti-aliasing, to get a pixelized line."
msgstr ""
