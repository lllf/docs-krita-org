# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:23+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:1
msgid "How to use layer styles in Krita."
msgstr "Hur lagerstilar används i Krita."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
#: ../../reference_manual/layers_and_masks/layer_styles.rst:17
msgid "Layer Styles"
msgstr "Lagerstilar"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "Layer Effects"
msgstr "Lagereffekter"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "Layer FX"
msgstr "Lagereffekt"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "ASL"
msgstr "ASL"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:19
msgid ""
"Layer styles are effects that are added on top of your layer. They are "
"editable and can easily be toggled on and off. To add a layer style to a "
"layer go to :menuselection:`Layer --> Layer Style`. You can also right-click "
"a layer to access the layer styles."
msgstr ""
"Lagerstilar är effekter som läggs till ovanpå ett lager. De är redigerbara "
"och kan enkelt sättas på eller stängas av. Gå till :menuselection:`Layer --> "
"Layer Style` för att lägga till en lagerstil. Det går också att högerklicka "
"på ett lager för att komma åt lagerstilarna."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:22
msgid ""
"When you have the layer styles window up, make sure that the :guilabel:"
"`Enable Effects` item is checked."
msgstr ""
"När fönstret med lagerstilar visas, säkerställ att :guilabel:`Aktivera "
"effekter` är markerat."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:24
msgid ""
"There are a variety of effects and styles you can apply to a layer. When you "
"add a style, your layer docker will show an extra \"Fx\" icon. This allows "
"you to toggle the layer style effects on and off."
msgstr ""
"Det finns en mängd olika effekter och stilar som man kan använda på ett "
"lager. När en stil läggs till, visar lagerpanelen en extra \"Fx\" ikon. Det "
"gör det möjligt att sätta på eller stänga av lagerstileffekterna."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:30
msgid ""
"This feature was added to increase support for :program:`Adobe Photoshop`. "
"The features that are included mirror what that application supports."
msgstr ""
"Funktionen har lagts till för att öka stöd för :program:`Adobe Photoshop`. "
"Funktionerna som är inkluderade speglar vad det programmet stödjer."
