# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:27+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:1
msgid "How to use transformation masks in Krita."
msgstr "Hur man använder transformeringsmasker i Krita."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:11
msgid "Layers"
msgstr "Lager"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:11
msgid "Masks"
msgstr "Masker"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:11
msgid "Transform"
msgstr "Transformera"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:16
msgid "Transformation Masks"
msgstr "Transformeringsmasker"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:18
msgid ""
"Rather than working with a brush to affect the mask, transformation masks "
"allow you to transform (move, rotate, shear, scale and perspective) a layer "
"without applying the transform directly to the paint layer and making it "
"permanent."
msgstr ""
"Istället för att arbeta med en pensel för att påverka masken, låter "
"transformeringsmasker dig transformera (flytta, rotera, skjuva, skala och "
"perspektivomforma) ett lager utan att direkt använda transformeringen på "
"målarlagret och göra den permanent."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:20
msgid ""
"In the same way that Filter and Transparency Masks can be attached to a "
"Paint layer and are non-destructive, so too can the Transformation Mask."
msgstr ""
"På samma sätt som filter- och genomskinlighetsmasker kan bifogas till ett "
"målarlager och är icke-destruktiva, är också transformeringsmasken det."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:23
msgid "Adding a Transformation Mask"
msgstr "Lägga till en transformeringsmask"

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:25
msgid "First add a transform mask to an existing layer."
msgstr "Lägg först till en transformeringsmask i ett befintligt lager."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:26
msgid "Select the transformation tool."
msgstr "Välj transformeringsverktyget."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:27
msgid ""
"Select any of the transform modes in the Tools Options dock and, with the "
"transform mask selected, apply them on the layer."
msgstr ""
"Välj någon av transformeringsmetoderna i panelen Verktygsalternativ, och "
"använd dem på lagret med transformeringsmasken vald."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:28
msgid "Hit apply."
msgstr "Klicka på Verkställ."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:29
msgid ""
"Toggle the transform visibility to see the difference between the original "
"and the transform applied."
msgstr ""
"Ändra transformens synlighet för att se skillnaden mellan originalet och när "
"transformeringen används."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:33
msgid ""
"Affine transforms, like Move, Rotate, Shear, Scale and Perspective get "
"updated instantly once the original is updated. Other transforms like Warp, "
"Cage and Liquify take up much more processing power, and to not to waste "
"that, Krita only updates those every three seconds."
msgstr ""
"Affina transformeringar, som Flytta, Rotera, Skjuva, Skala och Perspektiv "
"uppdateras omedelbart så fort originalen uppdateras. Övriga transformer som "
"Förvrid,  Bur och Gör flytande upptar mycket mer processorkraft, och för att "
"inte slösa med den uppdaterar Krita dem bara var tredje sekund."

#: ../../reference_manual/layers_and_masks/transformation_masks.rst:35
msgid ""
"To edit a transform, select the transform mask, and try to use the transform "
"tool on the layer. The transform mode will be the same as the stored "
"transform, regardless of what transform you had selected. If you switch "
"transform modes, the transformation will be undone."
msgstr ""
"Välj transformeringsmasken och försök använda transformeringsverktyget på "
"lagret för att redigera en transformering. Transformeringsmetoden är samma "
"som den lagrade transformeringen, oberoende av vilken transformering som var "
"vald. Om man byter transformeringsmetod, ångras transformeringen."
