msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___colors___linear_and_gamma.pot\n"

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/trc_gray_gradients.svg"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/Basicreading3trcsv2.svg"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/red_green_mixes_trc.svg"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/3trcsresult.png"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:1
msgid "The effect of gamma and linear."
msgstr "Gamma 曲线和线性曲线对颜色的影响。"

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Gamma"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Linear Color Space"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Linear"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Tone Response curve"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "EOTF"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Transfer Curve"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:17
msgid "Gamma and Linear"
msgstr "Gamma 曲线和线性曲线"

#: ../../general_concepts/colors/linear_and_gamma.rst:19
msgid ""
"Now, the situation we talk about when talking theory is what we would call "
"'linear'. Each step of brightness is the same value. Our eyes do not "
"perceive linearly. Rather, we find it more easy to distinguish between "
"darker grays than we do between lighter grays."
msgstr ""
"我们在讨论色彩理论时，“线性”指的是每级亮度变化区间均保持一致的情况。然而肉眼"
"对亮度的感知却不是线性的。我们发现比起在亮部的灰色，人们更容易区分暗部的灰"
"色。"

#: ../../general_concepts/colors/linear_and_gamma.rst:22
msgid ""
"As humans are the ones using computers, we have made it so that computers "
"will give more room to darker values in the coordinate system of the image. "
"We call this 'gamma-encoding', because it is applying a gamma function to "
"the TRC or transfer function of an image. The TRC in this case being the "
"Tone Response Curve or Tone Reproduction Curve or Transfer function (because "
"color management specialists hate themselves), which tells your computer or "
"printer how much color corresponds to a certain value."
msgstr ""
"由于计算机是供人使用的，为了迁就人类视觉的特点，我们在图像的色彩坐标系统里为"
"暗部的颜色预留了更多空间。这种调整发生在图像 TRC 的 gamma 函数上，所以我们把"
"它叫做“gamma 编码”。此处的 TRC 的含义可以是 **阶调响应曲线** 、也可以是阶调重"
"现曲线、也可以是转换函数之类的东西 (色彩管理专家爱跟自己过不去)，它们的作用是"
"告诉计算机或打印机某个颜色数值应该怎样去进行响应。"

#: ../../general_concepts/colors/linear_and_gamma.rst:28
msgid ".. image:: images/color_category/Pepper_tonecurves.png"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:28
msgid ""
"One of the most common issues people have with Krita's color management is "
"the assigning of the right colorspace to the encoded TRC. Above, the center "
"Pepper is the right one, where the encoded and assigned TRC are the same. To "
"the left we have a Pepper encoded in sRGB, but assigned a linear profile, "
"and to the right we have a Pepper encoded with a linear TRC and assigned an "
"sRGB TRC. Image from `Pepper & Carrot <https://www.peppercarrot.com/>`_."
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:30
msgid ""
"The following table shows how there's a lot of space being used by lighter "
"values in a linear space compared to the default sRGB TRC of our modern "
"computers and other TRCs available in our delivered profiles:"
msgstr ""
"下图展示了各种色彩空间的 TRC 和亮度的关系。第二行的 sRGB TRC 被广泛用于当代计"
"算机环境。第一行的线性空间分配给浅色的空间要远大于 sRGB TRC 特性文件以及 "
"Krita 自带的其他 TRC 特性文件。"

#: ../../general_concepts/colors/linear_and_gamma.rst:35
msgid ""
"If you look at linear of Rec. 709 TRCs, you can see there's quite a jump "
"between the darker shades and the lighter shades, while if we look at the "
"Lab L* TRC or the sRGB TRC, which seem more evenly spaced. This is due to "
"our eyes' sensitivity to darker values. This also means that if you do not "
"have enough bit depth, an image in a linear space will look as if it has "
"ugly banding. Hence why, when we make images for viewing on a screen, we "
"always use something like the Lab L\\*, sRGB or Gamma 2.2 TRCs to encode the "
"image with."
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:38
msgid ""
"However, this modification to give more space to darker values does lead to "
"wonky color maths when mixing the colors."
msgstr "但是，这种针对人眼特性的非线性修正会在混色时干扰色彩运算。"

#: ../../general_concepts/colors/linear_and_gamma.rst:40
msgid "We can see this with the following experiment:"
msgstr "我们可以通过下面的实验来演示："

#: ../../general_concepts/colors/linear_and_gamma.rst:46
msgid ""
".. image:: images/color_category/Krita_2_9_colormanagement_blending_1.png"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:46
msgid ""
"**Left:** Colored circles blurred in a regular sRGB space. **Right:** "
"Colored circles blurred in a linear space."
msgstr "**左图** 常规 sRGB 空间下的模糊效果； **右图** 线性空间下的模糊效果。"

#: ../../general_concepts/colors/linear_and_gamma.rst:48
msgid ""
"Colored circles, half blurred. In a gamma-corrected environment, this gives "
"an odd black border. In a linear environment, this gives us a nice gradation."
msgstr ""
"我们使用滤镜对几个彩色的圆形进行半模糊处理。在 gamma 校正后的环境下，该操作会"
"产生奇怪的黑边。而在线性环境下，该处理给出了自然的渐变效果。"

#: ../../general_concepts/colors/linear_and_gamma.rst:50
msgid "This also counts for Krita's color smudge brush:"
msgstr "Krita 的颜色涂抹笔刷引擎也受其影响："

#: ../../general_concepts/colors/linear_and_gamma.rst:56
msgid ""
".. image:: images/color_category/Krita_2_9_colormanagement_blending_2.png"
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:56
msgid ""
"That's right, the 'muddying' of colors as is a common complaint by digital "
"painters everywhere, is in fact, a gamma-corrected colorspace mucking up "
"your colors. If you had been working in LAB to avoid this, be sure to try "
"out a linear rgb color space."
msgstr ""
"看到这里是不是感觉恍然大悟？“颜色很脏”这个数字画手常有的抱怨，实质上是 gamma "
"校正后的色彩空间扭曲了颜色。如果你想要在 LAB 空间中工作以避免这个问题，记得要"
"使用一个线性的色彩空间。"

#: ../../general_concepts/colors/linear_and_gamma.rst:59
msgid "What is happening under the hood"
msgstr "不同 TRC 影响颜色运算的原理"

#: ../../general_concepts/colors/linear_and_gamma.rst:62
msgid "Imagine we want to mix red and green."
msgstr "现在请想象我们要对红和绿进行混色。"

#: ../../general_concepts/colors/linear_and_gamma.rst:64
msgid ""
"First, we would need the color coordinates of red and green inside our color "
"space's color model. So, that'd be..."
msgstr "首先，让我们确定红和绿在选定的色彩空间和色彩模型中的坐标。它们是："

#: ../../general_concepts/colors/linear_and_gamma.rst:67
msgid "Color"
msgstr "颜色"

#: ../../general_concepts/colors/linear_and_gamma.rst:67
#: ../../general_concepts/colors/linear_and_gamma.rst:69
#: ../../general_concepts/colors/linear_and_gamma.rst:76
#: ../../general_concepts/colors/linear_and_gamma.rst:78
msgid "Red"
msgstr "红"

#: ../../general_concepts/colors/linear_and_gamma.rst:67
#: ../../general_concepts/colors/linear_and_gamma.rst:70
#: ../../general_concepts/colors/linear_and_gamma.rst:76
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "Green"
msgstr "绿"

#: ../../general_concepts/colors/linear_and_gamma.rst:67
#: ../../general_concepts/colors/linear_and_gamma.rst:82
msgid "Blue"
msgstr "蓝"

#: ../../general_concepts/colors/linear_and_gamma.rst:69
#: ../../general_concepts/colors/linear_and_gamma.rst:70
#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "1.0"
msgstr "1.0"

#: ../../general_concepts/colors/linear_and_gamma.rst:69
#: ../../general_concepts/colors/linear_and_gamma.rst:70
#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
#: ../../general_concepts/colors/linear_and_gamma.rst:82
msgid "0.0"
msgstr "0.0"

#: ../../general_concepts/colors/linear_and_gamma.rst:73
msgid "We then average these coordinates over three mixes:"
msgstr "接着我们给三种不同的混色组合分配坐标值："

#: ../../general_concepts/colors/linear_and_gamma.rst:76
msgid "Mix1"
msgstr "混色 1"

#: ../../general_concepts/colors/linear_and_gamma.rst:76
msgid "Mix2"
msgstr "混色 2"

#: ../../general_concepts/colors/linear_and_gamma.rst:76
msgid "Mix3"
msgstr "混色 3"

#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "0.75"
msgstr "0.75"

#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "0.5"
msgstr "0.5"

#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "0.25"
msgstr "0.25"

#: ../../general_concepts/colors/linear_and_gamma.rst:85
msgid ""
"But to figure out how these colors look on screen, we first put the "
"individual values through the TRC of the color-space we're working with:"
msgstr ""
"为了得出这些颜色在屏幕上的显示效果，我们要先把每组数值通过当前色彩空间的 TRC "
"进行换算："

#: ../../general_concepts/colors/linear_and_gamma.rst:93
msgid ""
"Then we fill in the values into the correct spot. Compare these to the "
"values of the mixture table above!"
msgstr ""
"然后我们将得到的数值填入相应的位置。不难发现，下表的最终数值和一开始的混色表"
"格的数值存在较大差异。"

#: ../../general_concepts/colors/linear_and_gamma.rst:99
msgid ""
"And this is why color mixtures are lighter and softer in linear space. "
"Linear space is more physically correct, but sRGB is more efficient in terms "
"of space, so hence why many images have an sRGB TRC encoded into them. In "
"case this still doesn't make sense: *sRGB gives largely* **darker** *values "
"than linear space for the same coordinates*."
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:102
msgid ""
"So different TRCs give different mixes between colors, in the following "
"example, every set of gradients is in order a mix using linear TRC, a mix "
"using sRGB TRC and a mix using Lab L* TRC."
msgstr ""

#: ../../general_concepts/colors/linear_and_gamma.rst:110
msgid ""
"So, you might be asking, how do I tick this option? Is it in the settings "
"somewhere? The answer is that we have several ICC profiles that can be used "
"for this kind of work:"
msgstr ""
"现在你可能会想问：怎样才能启用线性空间？有一个选项来控制它吗？答案是：你要通"
"过特定的 ICC 特性文件来使用线性空间。Krita 已经自带了一系列线性 ICC 特性文"
"件："

#: ../../general_concepts/colors/linear_and_gamma.rst:112
msgid "scRGB (linear)"
msgstr "scRGB (linear)"

#: ../../general_concepts/colors/linear_and_gamma.rst:113
msgid "All 'elle'-profiles ending in 'g10', such as *sRGB-elle-v2-g10.icc*."
msgstr "elle 系列中所有后缀为“g10”的特性文件，如 *sRGB-elle-v2-g10.icc* 等。"

#: ../../general_concepts/colors/linear_and_gamma.rst:115
msgid ""
"In fact, in all the 'elle'-profiles, the last number indicates the gamma. "
"1.0 is linear, higher is gamma-corrected and 'srgbtrc' is a special gamma "
"correction for the original sRGB profile."
msgstr ""
"每个“elle”系列特性文件最后的数字代表了该空间的 gamma 值。1.0 为线性空间，大"
"于 1.0 就是 gamma 校正空间。“srgbtrc”是初版 sRGB 特性文件所采用的特殊的 "
"gamma 校正值。"

#: ../../general_concepts/colors/linear_and_gamma.rst:117
msgid ""
"If you use the color space browser, you can tell the TRC from the 'estimated "
"gamma'(if it's 1.0, it's linear), or from the TRC widget in Krita 3.0, which "
"looks exactly like the curve graphs above."
msgstr ""
"在使用色彩空间浏览器时，你可以从“估算 gamma 值”一项看出该空间是否为 TRC 空间 "
"(如果是 1.0 则为线性空间)。Krita 3.0 里面可在TRC 工具列找到此项，它看起来和上"
"方的曲线图一模一样。"

#: ../../general_concepts/colors/linear_and_gamma.rst:119
msgid ""
"Even if you do not paint much, but are for example making textures for a "
"videogame or rendering, using a linear space is very beneficial and will "
"speed up the renderer a little, for it won't have to convert images on its "
"own."
msgstr ""
"即使你不怎么绘画，只是需要为视频游戏和 3D 渲染制作材质，使用线性空间依然可以"
"减少引擎内的转换环节，从而提高渲染速度。"

#: ../../general_concepts/colors/linear_and_gamma.rst:121
msgid ""
"The downside of linear space is of course that white seems very overpowered "
"when mixing with black, because in a linear space, light grays get more "
"room. In the end, while linear space is physically correct, and a boon to "
"work in when you are dealing with physically correct renderers for "
"videogames and raytracing, Krita is a tool and no-one will hunt you down for "
"preferring the dark mixing of the sRGB TRC."
msgstr ""
"线性空间有一个最为明显的缺点：把一种颜色与黑、白混色时，黑色的效果非常不明"
"显，而白色的效果非常强势，这是因为在线性空间中浅灰色所占的比例比深灰色要大得"
"多。说到底，尽管线性空间在物理学上是准确的，在配合基于物理学的游戏渲染引擎和"
"光线追踪时效果更好，但 Krita 毕竟是一个绘画工具，如果你习惯了在符合人类感知特"
"点的 sRGB TRC 空间下面进行混色，没有人会因此刁难你。"
