# Translation of docs_krita_org_reference_manual___brushes___brush_settings___opacity_and_flow.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_settings___opacity_and_flow\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 08:25+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:1
msgid "Opacity and flow in Krita."
msgstr "Непрозорість і потік у Krita."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:27
msgid "Opacity"
msgstr "Непрозорість"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:30
msgid "Flow"
msgstr "Потік"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
msgid "Transparency"
msgstr "Прозорість"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:17
msgid "Opacity and Flow"
msgstr "Непрозорість і потік"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:19
msgid "Opacity and flow are parameters for the transparency of a brush."
msgstr "Непрозорість і потік є параметрами прозорості пензля."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:22
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Flow.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Flow.png"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:23
msgid "They are interlinked with the painting mode setting."
msgstr "Ці параметри пов'язано із параметром режиму малювання."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:26
msgid ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_02.png"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:28
msgid "The transparency of a stroke."
msgstr "Прозорість мазка."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:30
msgid ""
"The transparency of separate dabs. Finally separated from Opacity in 2.9."
msgstr ""
"Прозорість окремих мазків. Відокремлено від параметра «Непрозорість» у "
"версії 2.9."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:34
msgid ".. image:: images/brushes/Krita_4_2_brushengine_opacity-flow_01.svg"
msgstr ".. image:: images/brushes/Krita_4_2_brushengine_opacity-flow_01.svg"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:35
msgid ""
"In Krita 4.1 and below, the flow and opacity when combined with brush "
"sensors would add up to one another, being only limited by the maximum "
"opacity. This was unexpected compared to all other painting applications, so "
"in 4.2 this finally got corrected to the flow and opacity multiplying, "
"resulting in much more subtle strokes. This change can be switched back in "
"the :ref:`tool_options_settings`, but we will be deprecating the old way in "
"future versions."
msgstr ""
"У версії Krita 4.1 і попередніх версіях потік та непрозорість у поєднанні із "
"датчиками пензля призводили до додавання ефектів, яке обмежувало лише "
"максимальне значення непрозорості. Така поведінка радикально відрізнялася "
"від інших програм для малювання, тому у версії 4.2 цю ваду було виправлено "
"так, що потік і непрозорість множаться, даючи делікатніші мазки. Нівелювати "
"наслідки цієї зміни можна за допомогою панелі :ref:`tool_options_settings`, "
"але ми вилучимо цей застарілий спосіб малювання у наступних версіях програми."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:38
msgid "The old behavior can be simulated in the new system by..."
msgstr "Стару поведінку у новій системі можна імітувати…"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:40
msgid "Deactivating the sensors on opacity."
msgstr "Вимиканням датчиків для непрозорості."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:41
msgid "Set the maximum value on flow to 0.5."
msgstr "Встановленням максимального значення потоку 0.5."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:42
msgid "Adjusting the pressure curve to be concave."
msgstr "Коригуванням кривої тиску так, щоб вона стала увігнутою."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:44
msgid ".. image:: images/brushes/flow_opacity_adapt_flow_preset.gif"
msgstr ".. image:: images/brushes/flow_opacity_adapt_flow_preset.gif"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:49
msgid "Painting mode"
msgstr "Режим малювання"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:51
msgid "Build-up"
msgstr "Надбудова"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:52
msgid "Will treat opacity as if it were the same as flow."
msgstr "Вважати непрозорість такою самою як і потік."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:54
msgid "Wash"
msgstr "Змивання"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:54
msgid "Will treat opacity as stroke transparency instead of dab-transparency."
msgstr ""
"Вважати непрозорість параметром прозорості штриха, а не прозорістю мазка."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:57
msgid ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_03.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_03.png"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:58
msgid ""
"Where the other images of this page had all three strokes set to painting "
"mode: wash, this one is set to build-up."
msgstr ""
"Для інших зображень на цій сторінці для усіх трьох штрихів встановлено режим "
"малювання «Змивання», але для цього зображення режимом є «Надбудова»."
