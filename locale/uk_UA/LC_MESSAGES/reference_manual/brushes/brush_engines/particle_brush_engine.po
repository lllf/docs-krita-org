# Translation of docs_krita_org_reference_manual___brushes___brush_engines___particle_brush_engine.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___particle_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:25+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<generated>:1
msgid "Iterations"
msgstr "Ітерації"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:1
msgid "The Particle Brush Engine manual page."
msgstr "Сторінка підручника щодо рушія пензлів часток."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:16
msgid "Particle Brush Engine"
msgstr "Рушій пензлів часток"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Рушій пензлів"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:19
msgid ".. image:: images/icons/particlebrush.svg"
msgstr ".. image:: images/icons/particlebrush.svg"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:20
msgid ""
"A brush that draws wires using parameters. These wires always get more "
"random and crazy over drawing distance. Gives very intricate lines best used "
"for special effects."
msgstr ""
"Пензель, який малює тонкі лінії на основі вказаних параметрів. Ці лінії "
"завжди набувають певної випадковості зі зростанням довжини мазка. Створює "
"дуже несподівані лінії, якими варто користуватися для спеціальних ефектів."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:23
msgid "Options"
msgstr "Параметри"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:24
msgid ":ref:`option_size_particle`"
msgstr ":ref:`option_size_particle`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:25
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:26
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:27
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:32
msgid "Brush Size"
msgstr "Розмір пензля"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:34
msgid "Particles"
msgstr "Частки"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:35
msgid "How many particles there's drawn."
msgstr "Скільки часток буде намальовано."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:36
msgid "Opacity Weight"
msgstr "Вага непрозорості"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:37
msgid "The Opacity of all particles. Is influenced by the painting mode."
msgstr "Непрозорість усіх часток. На неї впливає режим малювання."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:38
msgid "Dx Scale (Distance X Scale)"
msgstr "Масштаб dx (масштаб відстані за X)"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:39
msgid ""
"How much the horizontal cursor distance affects the placing of the pixel. Is "
"unstable on negative values. 1.0 is equal."
msgstr ""
"Визначає, наскільки відстань від курсора за горизонталлю впливає на "
"розташування пікселя. Є нестабільним для від'ємних значень. 1.0 — "
"рівномірний масштаб."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:40
msgid "Dy Scale (Distance Y Scale)"
msgstr "Масштаб dy (масштаб відстані за Y)"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:41
msgid ""
"How much the vertical cursor distance affects the placing of the pixel. Is "
"unstable on negative values. 1.0 is equal."
msgstr ""
"Визначає, наскільки відстань від курсора за вертикаллю впливає на "
"розташування пікселя. Є нестабільним для від'ємних значень. 1.0 — "
"рівномірний масштаб."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:42
msgid "Gravity"
msgstr "Тяжіння"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:43
msgid ""
"Multiplies with the previous particle's position, to find the new particle's "
"position."
msgstr ""
"Множиться із попереднім розташуванням частки для визначення нового "
"розташування частки."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:45
msgid ""
"The higher, the higher the internal acceleration is, with the furthest away "
"particle from the brush having the highest acceleration. This means that the "
"higher iteration is, the faster and more randomly a particle moves over "
"time, giving a messier result."
msgstr ""
"Чим більшим буде це значення, тим більшим буде внутрішнє прискорення, "
"причому найвіддаленіша частка від пензля прискорюватиметься найбільше. Це "
"означає, що чим більшою буде ітерація, тим швидше і хаотичніше рухатиметься "
"частка з часом, створюючи вигадливіші візерунки."
