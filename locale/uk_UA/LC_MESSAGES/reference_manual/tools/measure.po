# Translation of docs_krita_org_reference_manual___tools___measure.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___measure\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 15:35+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:52
msgid ""
".. image:: images/icons/measure_tool.svg\n"
"   :alt: toolmeasure"
msgstr ""
".. image:: images/icons/measure_tool.svg\n"
"   :alt: toolmeasure"

#: ../../reference_manual/tools/measure.rst:1
msgid "Krita's measure tool reference."
msgstr "Довідник із інструмента вимірювання Krita."

#: ../../reference_manual/tools/measure.rst:11
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/measure.rst:11
msgid "Measure"
msgstr "Вимірювання"

#: ../../reference_manual/tools/measure.rst:11
msgid "Angle"
msgstr "Кут"

#: ../../reference_manual/tools/measure.rst:11
msgid "Compass"
msgstr "Компас"

#: ../../reference_manual/tools/measure.rst:16
msgid "Measure Tool"
msgstr "Інструмент «Вимірювання»"

#: ../../reference_manual/tools/measure.rst:18
msgid "|toolmeasure|"
msgstr "|toolmeasure|"

#: ../../reference_manual/tools/measure.rst:20
msgid ""
"This tool is used to measure distances and angles. Click the |mouseleft| to "
"indicate the first endpoint or vertex of the angle, keep the button pressed, "
"drag to the second endpoint and release the button. The results will be "
"shown on the Tool Options docker. You can choose the length units from the "
"drop-down list."
msgstr ""
"Цей інструмент використовують для вимірювання відстаней і кутів. Натисніть |"
"mouseleft|, щоб позначити першу точку або вершину кута, і перетягніть "
"вказівник у другу точку. Результат вимірювання буде показано на бічній "
"панелі параметрів інструмента. Вибрати одиницю довжини для вимірювання можна "
"за допомогою спадного списку."

#: ../../reference_manual/tools/measure.rst:23
msgid "Tool Options"
msgstr "Параметри інструмента"

#: ../../reference_manual/tools/measure.rst:25
msgid ""
"The measure tool-options allow you to change between the units used. Unit "
"conversion varies depending on the DPI setting of a document."
msgstr ""
"За допомогою параметрів інструмента вимірювання ви можете визначити одиниці, "
"у яких виконуватиметься вимірювання. Перетворенням між одиницями "
"виконуватиметься відповідно до параметрів роздільності зображення у "
"документі."
