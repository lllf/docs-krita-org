# Translation of docs_krita_org_reference_manual___tools___smart_patch.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___smart_patch\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:34+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:62
msgid ""
".. image:: images/icons/smart_patch_tool.svg\n"
"   :alt: toolsmartpatch"
msgstr ""
".. image:: images/icons/smart_patch_tool.svg\n"
"   :alt: toolsmartpatch"

#: ../../reference_manual/tools/smart_patch.rst:1
msgid "Krita's smart patch tool reference."
msgstr "Довідник із інструмента кмітливого накладання Krita."

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Smart Patch"
msgstr "Кмітливе накладання"

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Automatic Healing"
msgstr "Автоматичне лікування"

#: ../../reference_manual/tools/smart_patch.rst:16
msgid "Smart Patch Tool"
msgstr "Інструмент кмітливого накладання"

#: ../../reference_manual/tools/smart_patch.rst:18
msgid "|toolsmartpatch|"
msgstr "|toolsmartpatch|"

#: ../../reference_manual/tools/smart_patch.rst:20
msgid ""
"The smart patch tool allows you to seamlessly remove elements from the "
"image. It does this by letting you draw the area which has the element you "
"wish to remove, and then it will attempt to use patterns already existing in "
"the image to fill the blank."
msgstr ""
"За допомогою інструмента кмітливого накладання ви можете безслідно вилучати "
"елементи з зображення. Інструмент надає вам змогу намалювати ділянку, на "
"якій розташовано елемент, який ви хочете вилучити, а потім намагається "
"скористатися зразками з зображення для заповнення порожнього місця."

#: ../../reference_manual/tools/smart_patch.rst:22
msgid "You can see it as a smarter version of the clone brush."
msgstr "Як можна бачити, це кмітливіша версія пензля клонування."

#: ../../reference_manual/tools/smart_patch.rst:25
msgid ".. image:: images/tools/Smart-patch.gif"
msgstr ".. image:: images/tools/Smart-patch.gif"

#: ../../reference_manual/tools/smart_patch.rst:26
msgid "The smart patch tool has the following tool options:"
msgstr "Для інструмента кмітливого накладання передбачено такі параметри:"

#: ../../reference_manual/tools/smart_patch.rst:29
msgid "Accuracy"
msgstr "Точність"

#: ../../reference_manual/tools/smart_patch.rst:31
msgid ""
"Accuracy indicates how many samples, and thus how often the algorithm is "
"run. A low accuracy will do few samples, but will also run the algorithm "
"fewer times, making it faster. Higher accuracy will do many samples, making "
"the algorithm run more often and give more precise results, but because it "
"has to do more work, it is slower."
msgstr ""
"Точність визначає кількість зразків зображення, а отже, частоту запуску "
"алгоритму. Низька точність означає використання лише декількох зразків, отже "
"меншу кількість запусків алгоритму і більшу швидкість. Для високої точності "
"використовується багато зразків і багато запусків алгоритму. Висока точність "
"забезпечує якісніші результати, але через потребу у значних обчисленнях "
"працює повільніше."

#: ../../reference_manual/tools/smart_patch.rst:34
msgid "Patch size"
msgstr "Розмір латки"

#: ../../reference_manual/tools/smart_patch.rst:36
msgid ""
"Patch size determines how big the size of the pattern to choose is. This "
"will be best explained with some testing, but if the surrounding image has "
"mostly small elements, like branches, a small patch size will give better "
"results, while a big patch size will be better for images with big elements, "
"so they get reused as a whole."
msgstr ""
"Розмір латки визначає, наскільки великими будуть вибрані програмою зразки. "
"Краще зрозуміти, як це працює можна за допомогою тестування. Якщо на "
"зображенні навколо латки багато малих елементів, наприклад гілок, малий "
"розмір латки забезпечить якісніші результати. Великий же розмір латки пасує "
"для зображень із великими елементами, які доцільніше використовувати цілими."
