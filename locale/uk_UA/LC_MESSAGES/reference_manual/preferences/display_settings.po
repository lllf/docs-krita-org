# Translation of docs_krita_org_reference_manual___preferences___display_settings.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___display_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-06 03:34+0200\n"
"PO-Revision-Date: 2019-05-06 07:09+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<generated>:1
msgid "Hide Layer thumbnail popup"
msgstr "Приховати контекстну підказку мініатюри шару"

#: ../../reference_manual/preferences/display_settings.rst:1
msgid "Display settings in Krita."
msgstr "Параметри показу у Krita."

#: ../../reference_manual/preferences/display_settings.rst:12
#: ../../reference_manual/preferences/display_settings.rst:24
msgid "OpenGL"
msgstr "OpenGL"

#: ../../reference_manual/preferences/display_settings.rst:12
#: ../../reference_manual/preferences/display_settings.rst:84
msgid "Canvas Border"
msgstr "Рамка полотна"

#: ../../reference_manual/preferences/display_settings.rst:12
msgid "Preferences"
msgstr "Налаштування"

#: ../../reference_manual/preferences/display_settings.rst:12
msgid "Settings"
msgstr "Параметри"

#: ../../reference_manual/preferences/display_settings.rst:12
msgid "Transparency Checkers"
msgstr "Пункти керування прозорістю"

#: ../../reference_manual/preferences/display_settings.rst:12
msgid "Canvas Graphics Acceleration"
msgstr "Прискорення графіки на полотні"

#: ../../reference_manual/preferences/display_settings.rst:12
msgid "Display"
msgstr "Показ"

#: ../../reference_manual/preferences/display_settings.rst:17
msgid "Display Settings"
msgstr "Параметри показу"

#: ../../reference_manual/preferences/display_settings.rst:20
msgid ".. image:: images/preferences/Krita_Preferences_Display.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Display.png"

#: ../../reference_manual/preferences/display_settings.rst:21
msgid "Here various settings for the rendering of Krita can be edited."
msgstr ""
"За допомогою цієї сторінки можна редагувати різноманітні параметри Krita, "
"які пов'язано із обробкою зображення."

#: ../../reference_manual/preferences/display_settings.rst:26
msgid ""
"**For Krita 3.3 or later: Reworded as \"*Canvas Graphics Acceleration*\"**"
msgstr ""
"**У Krita 3.3 і новіших версіях замінено на \"*Прискорення графіки на "
"полотні*\"**"

#: ../../reference_manual/preferences/display_settings.rst:28
msgid ""
"OpenGL is a bit of code especially for graphics cards. Graphics cards a "
"dedicate piece of hardware for helping your computer out with graphics "
"calculations, which Krita uses a lot. All modern computer have graphics "
"cards."
msgstr ""
"OpenGL — програмна бібліотека, яку спеціально створено для роботи з "
"відеокартками. Відеокартки — спеціальний тип обладнання, який допомагає "
"комп'ютеру виконувати обчислення, які пов'язано із виведенням графічних "
"даних на екран, тобто тип обладнання, який широко використовує Krita. Усі "
"сучасні комп'ютери обладнано відеокартками."

#: ../../reference_manual/preferences/display_settings.rst:30
msgid ""
"**For Krita 3.3 or later:** On Windows, Krita also supports using Direct3D "
"instead with the help of the ANGLE library. ANGLE works by converting the "
"OpenGL functions that Krita makes use of to the equivalent in Direct3D. It "
"may (or may not) be slower than native OpenGL, but it has better "
"compatibility with typical Windows graphics drivers."
msgstr ""
"**Krita 3.3 або новіші версії:** у Windows у Krita також передбачено "
"підтримку використання Direct3D на основі бібліотеки ANGLE. ANGLE виконує "
"свою роботу, транслюючи функції OpenGL, які використовує Krita у "
"еквівалентні функції Direct3D. Цей спосіб може працювати повільніше (чи "
"швидше) за OpenGL, але точно є суміснішим для типових драйверів до "
"відеокарток у Windows."

#: ../../reference_manual/preferences/display_settings.rst:32
msgid ""
"Enable OpenGL **(For Krita 3.3 or later: Reworded as *Canvas Graphics "
"Acceleration*)**"
msgstr ""
"Увімкнути OpenGL **(У Krita 3.3 і новіших версіях замінено на *Прискорення "
"графіки на полотні*)**"

#: ../../reference_manual/preferences/display_settings.rst:33
msgid ""
"Selecting this checkbox will enable the OpenGL / ANGLE canvas drawing mode. "
"With a decent graphics card this should give faster feedback on brushes and "
"tools. Also the canvas operations like Rotate, Zoom and Pan should be "
"considerably faster."
msgstr ""
"Позначення цього пункту увімкне режим малювання полотна за допомогою "
"OpenGL / ANGLE. Якщо ви користуєтеся якісною відеокарткою, це має "
"пришвидшити роботу пензлів та інших інструментів програми. Крім того, має "
"бути значно пришвидшено роботу дій з полотном, зокрема обертання, "
"масштабування та панорамування."

#: ../../reference_manual/preferences/display_settings.rst:35
msgid "For Krita 3.3 or later:"
msgstr "У Krita 3.3 та новіших версіях:"

#: ../../reference_manual/preferences/display_settings.rst:35
msgid "Renderer"
msgstr "Засіб показу"

#: ../../reference_manual/preferences/display_settings.rst:36
msgid ""
"*On Windows:* You can switch between native OpenGL or ANGLE Direct3D 11 "
"rendering. The usual recommendation is to leave it as \"Auto\", which Krita "
"will decide the best to use based on some internal compatibility checking. "
"Changes to this option require a restart of Krita to take effect."
msgstr ""
"*У Windows:* ви можете перемикати систему між використанням OpenGL та "
"обробкою зображення за допомогою Direct3D 11 у ANGLE. Звичайною "
"рекомендацією є використання значення :guilabel:`Авто`, яке надасть змогу "
"Krita вибрати найкращий варіант на основі вбудованої перевірки сумісності. "
"Для набуття чинності зміна значення у цьому пункті потребуватиме перезапуску "
"Krita."

#: ../../reference_manual/preferences/display_settings.rst:37
msgid "Use Texture Buffer"
msgstr "Використовувати буфер текстур"

#: ../../reference_manual/preferences/display_settings.rst:38
msgid ""
"This setting utilizes the graphics card's buffering capabilities to speed "
"things up a bit. Although for now, this feature may be broken on some AMD/"
"Radeon cards and may work fine on some Intel graphics cards."
msgstr ""
"За допомогою цього пункту можна наказати системі використати можливості "
"відеокартки з буферизації для певного пришвидшення обробки. Хоча у поточній "
"версії ця можливість може працювати недостатньо якісно на деяких картках AMD/"
"Radeon, вона добре працює на деяких відеокартках Intel."

#: ../../reference_manual/preferences/display_settings.rst:40
msgid ""
"The user can choose which scaling mode to use while zooming the canvas. The "
"choice here only affects the way the image is displayed during canvas "
"operations and has no effect on how Krita scales an image when a "
"transformation is applied."
msgstr ""
"Користувач може вибрати режим масштабування, який буде використано для зміни "
"масштабу на полотні. Вибраний за допомогою цього пункту варіант "
"стосуватиметься лише способу показу зображення під час виконання дій з "
"полотном і не впливатиме на те, як Krita масштабуватиме зображення при "
"заостосуванні перетворення."

#: ../../reference_manual/preferences/display_settings.rst:42
msgid "Nearest Neighbour"
msgstr "Найближчий сусідній"

#: ../../reference_manual/preferences/display_settings.rst:43
msgid ""
"This is the fastest and crudest filtering method. While fast, this results "
"in a large number of artifacts - 'blockiness' during magnification, and "
"aliasing and shimmering during minification."
msgstr ""
"Це найшвидший і найгрубіший спосіб фільтрування. Хоча спосіб і є швидким, "
"його застосування призводить до великої кількості дефектів — появи «блоків» "
"під час збільшення та злиття і блимання під час зменшення."

#: ../../reference_manual/preferences/display_settings.rst:44
msgid "Bilinear Filtering"
msgstr "Білінійне фільтрування"

#: ../../reference_manual/preferences/display_settings.rst:45
msgid ""
"This is the next step up. This removes the 'blockiness' seen during "
"magnification and gives a smooth looking result. For most purposes this "
"should be a good trade-off between speed and quality."
msgstr ""
"Це наступний крок. Надає змогу уникнути появи блоків, які помітні під час "
"збільшення, і дає згладжений на вигляд результат. Для більшості випадків це "
"має бути непоганий компроміс між швидкістю і якістю."

#: ../../reference_manual/preferences/display_settings.rst:46
msgid "Trilinear Filtering"
msgstr "Трилінійне фільтрування"

#: ../../reference_manual/preferences/display_settings.rst:47
msgid "This should give a little better result than Bilinear Filtering."
msgstr "Має давати кращий результат за білінійне фільтрування."

#: ../../reference_manual/preferences/display_settings.rst:49
msgid "Scaling Mode"
msgstr "Режим масштабування"

#: ../../reference_manual/preferences/display_settings.rst:49
msgid "High Quality Filtering"
msgstr "Високоякісне фільтрування"

#: ../../reference_manual/preferences/display_settings.rst:49
msgid ""
"Only available when your graphics card supports OpenGL 3.0. As the name "
"suggests, this setting provides the best looking image during canvas "
"operations."
msgstr ""
"Цей пункт доступний, лише якщо у драйверах до вашої відеокартки передбачено "
"підтримку OpenGL 3.0. Як можна зрозуміти із назви, за допомогою цього пункту "
"можна досягти найякіснішого зображення під час дій з полотном."

#: ../../reference_manual/preferences/display_settings.rst:54
msgid "HDR"
msgstr "HDR"

#: ../../reference_manual/preferences/display_settings.rst:58
msgid "These settings are only available when using Windows."
msgstr "Цими параметрами можна скористатися лише у середовищі Windows."

#: ../../reference_manual/preferences/display_settings.rst:60
msgid ""
"Since 4.2 Krita can not just edit floating point images, but also render "
"them on screen in a way that an HDR capable setup can show them as HDR "
"images."
msgstr ""
"Починаючи з версії 4.2, Krita не просто редагує зображення із дійсними "
"значеннями кольорів, але і обробляє їх для показу на екрані так, що за "
"конфігурації із усіма можливостями HDR зображення було показано як справжні "
"зображення HDR."

#: ../../reference_manual/preferences/display_settings.rst:62
msgid ""
"The HDR settings will show you the display format that Krita can handle, and "
"the current output format. You will want to set the preferred output format "
"to the one closest to what your display can handle to make full use of it."
msgstr ""
"У параметрах HDR ви зможете побачити формат показу, з яким може працювати "
"Krita, і поточний формат виведення даних. Вам варто встановити бажаний "
"формат виведення даних якомога ближчим до того, який може бути використано "
"на вашому дисплеї, щоб повністю використати його можливості."

#: ../../reference_manual/preferences/display_settings.rst:64
msgid "Display Format"
msgstr "Формат показу"

#: ../../reference_manual/preferences/display_settings.rst:65
msgid ""
"The format your display is in by default. If this isn't higher than 8bit, "
"there's a good chance your monitor is not an HDR monitor as far as Krita can "
"tell. This can be a hardware issue, but also a graphics driver issue. Check "
"if other HDR applications, or the system HDR settings are configured "
"correctly."
msgstr ""
"Формат, у якому типово працює ваш дисплей. Якщо значення тут не перевищують "
"8-бітових, дуже ймовірно, що ваш монітор не може показувати кольори широкого "
"динамічного діапазону (HDR), принаймні, наскільки це відомо Krita. Причиною "
"цього можуть бути проблеми не лише із обладнанням, але і з драйверами до "
"графічної картки. Перевірте, чи працюють інші програми з можливостями показу "
"HDR та чи налаштовано належним чином загальносистемні параметри HDR."

#: ../../reference_manual/preferences/display_settings.rst:66
msgid "Current Output format"
msgstr "Поточний формат виведення"

#: ../../reference_manual/preferences/display_settings.rst:67
msgid "What Krita is rendering the canvas to currently."
msgstr "Визначає, що саме Krita обробляє для показу на полотні."

#: ../../reference_manual/preferences/display_settings.rst:69
msgid "Preferred Output Format"
msgstr "Бажаний формат виведення"

#: ../../reference_manual/preferences/display_settings.rst:69
msgid ""
"Which surface type you prefer. This should be ideally the closest to the "
"display format, but perhaps due to driver issues you might want to try other "
"formats. This requires a restart."
msgstr ""
"Визначає бажаний для вас тип поверхні. Ідеально, має бути якомога ближчим до "
"формату показу, але, ймовірно, через проблеми із драйвером вам можуть "
"знадобитися інші формати. Зміна значення цього параметра потребуватиме "
"перезапуску програми."

#: ../../reference_manual/preferences/display_settings.rst:72
msgid "Transparency Checkboxes"
msgstr "Пункти керування прозорістю"

#: ../../reference_manual/preferences/display_settings.rst:74
msgid ""
"Krita supports layer transparency. Of course, the nasty thing is that "
"transparency can't be seen. So to indicate transparency at the lowest layer, "
"we use a checker pattern. This part allows you to configure it."
msgstr ""
"У Krita передбачено підтримку прозорості шарів. Звичайно ж, проблемою є те, "
"що прозорість неможливо побачити. Отже, щоб позначити прозорість на "
"найнижчому шарі, ми використовуємо картатий візерунок, подібний до "
"шахівниці. За допомогою цієї частини панелі налаштувань ви можете "
"налаштувати цей картатий візерунок."

#: ../../reference_manual/preferences/display_settings.rst:76
msgid "Size"
msgstr "Розмір"

#: ../../reference_manual/preferences/display_settings.rst:77
msgid ""
"This sets the size of the checkers which show up in transparent parts of an "
"image."
msgstr ""
"Визначає розмір клітинок у картатому візерунку, який позначатиме прозорі "
"частини зображення."

#: ../../reference_manual/preferences/display_settings.rst:78
#: ../../reference_manual/preferences/display_settings.rst:86
#: ../../reference_manual/preferences/display_settings.rst:98
msgid "Color"
msgstr "Колір"

#: ../../reference_manual/preferences/display_settings.rst:79
msgid "The user can set the colors for the checkers over here."
msgstr "Тут можна вибрати кольори картатого замінника прозорих ділянок."

#: ../../reference_manual/preferences/display_settings.rst:81
msgid "Move Checkers When Scrolling"
msgstr "Пересувати шахівницю під час гортання"

#: ../../reference_manual/preferences/display_settings.rst:81
msgid ""
"When selected the checkers will move along with opaque elements of an image "
"during canvas Panning, Zooming, etc.  Otherwise the checkers remain "
"stationary and only the opaque parts of an image will move."
msgstr ""
"Якщо позначено, картате зображення прозорості пересуватиметься разом із "
"непрозорими елементами зображення під час панорамування полотном, зміни "
"масштабу тощо. Якщо ж пункт не буде позначено, картата шахівниця "
"лишатиметься стаціонарною, пересуватимуться лише непрозорі частини "
"зображення."

#: ../../reference_manual/preferences/display_settings.rst:87
msgid ""
"The user can select the color for the canvas i.e. the space beyond a "
"document's boundaries."
msgstr ""
"Користувач може вибрати колір полотна, тобто колір простору за межами "
"документа."

#: ../../reference_manual/preferences/display_settings.rst:89
msgid "Hide Scrollbars"
msgstr "Приховати смужки гортання"

#: ../../reference_manual/preferences/display_settings.rst:89
msgid "Selecting this will hide the scrollbars in all view modes."
msgstr ""
"Позначення цього пункту призведе до приховування смужок гортання в усіх "
"режимах перегляду."

#: ../../reference_manual/preferences/display_settings.rst:92
msgid "Pixel Grid"
msgstr "Піксельна сітка"

#: ../../reference_manual/preferences/display_settings.rst:96
msgid ""
"This allows configuring an automatic pixel-by-pixel grid, which is very "
"useful for doing pixel art."
msgstr ""
"Цей пункт надає змогу налаштувати автоматичну ґратку за пікселями, яка дуже "
"корисно для створення піксель-арту."

#: ../../reference_manual/preferences/display_settings.rst:99
msgid "The color of the grid."
msgstr "Колір ґратки."

#: ../../reference_manual/preferences/display_settings.rst:101
msgid "Start Showing at"
msgstr "Почати показ з"

#: ../../reference_manual/preferences/display_settings.rst:101
msgid ""
"This determines the zoom level at which the pixel grid starts showing, as "
"showing it when the image is zoomed out a lot will make the grid overwhelm "
"the image, and is thus counter productive."
msgstr ""
"Цей пункт визначає рівень масштабування, за якого програма почне показувати "
"піксельну ґратку. Показ ґратки за малих значень масштабу заважає показу "
"самого зображення, тому є позбавленим сенсу."

#: ../../reference_manual/preferences/display_settings.rst:104
msgid "Miscellaneous"
msgstr "Різне"

#: ../../reference_manual/preferences/display_settings.rst:106
msgid "Color Channels in Color"
msgstr "Канали кольорів у кольорі"

#: ../../reference_manual/preferences/display_settings.rst:107
msgid ""
"This is supposed to determine what to do when only a single channel is "
"selected in the channels docker, but it doesn't seem to work."
msgstr ""
"Цей пункт призначено для визначення поведінки програми, якщо на бічній "
"панелі каналів вибрано лише один канал."

#: ../../reference_manual/preferences/display_settings.rst:108
msgid "Enable Curve Anti-Aliasing"
msgstr "Увімкнути згладжування кривих"

#: ../../reference_manual/preferences/display_settings.rst:109
msgid ""
"This allows anti-aliasing on previewing curves, like the ones for the circle "
"tool, or the path tool."
msgstr ""
"Цей пункт уможливлює використання згладжування на кривих попереднього "
"перегляду, зокрема для інструментів побудови кіл та контурів."

#: ../../reference_manual/preferences/display_settings.rst:110
msgid "Enable Selection Outline Anti-Aliasing"
msgstr "Увімкнути згладжування контуру позначеного"

#: ../../reference_manual/preferences/display_settings.rst:111
msgid ""
"This allows automatic anti-aliasing on selection. It makes the selection "
"feel less jaggy and more precise."
msgstr ""
"Цей пункт уможливлює автоматичне згладжування на позначних ділянках. Надає "
"позначеним ділянкам плавнішого та точнішого вигляду."

#: ../../reference_manual/preferences/display_settings.rst:112
msgid "Hide window scrollbars."
msgstr "Приховати смужки гортання"

#: ../../reference_manual/preferences/display_settings.rst:113
msgid "Hides the scrollbars on the canvas."
msgstr "Приховує смужки гортання на полотні."

#: ../../reference_manual/preferences/display_settings.rst:115
msgid "This disables the thumbnail that you get when hovering over a layer."
msgstr ""
"За допомогою цього пункту можна вимкнути мініатюру, яку програма показує при "
"наведенні вказівника миші на пункт шару."
