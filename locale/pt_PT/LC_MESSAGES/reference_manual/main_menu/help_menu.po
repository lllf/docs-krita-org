msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 15:01+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Krita\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../<generated>:1
msgid "About KDE"
msgstr "Acerca do KDE"

#: ../../reference_manual/main_menu/help_menu.rst:1
msgid "The help menu in Krita."
msgstr "O menu de ajuda no Krita."

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "About"
msgstr "Acerca"

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Handbook"
msgstr "Manual"

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Bug"
msgstr "Erro"

#: ../../reference_manual/main_menu/help_menu.rst:16
msgid "Help Menu"
msgstr "O Menu Ajuda"

#: ../../reference_manual/main_menu/help_menu.rst:18
msgid "Krita Handbook"
msgstr "Manual do Krita"

#: ../../reference_manual/main_menu/help_menu.rst:19
msgid "Opens a browser and sends you to the index of this manual."
msgstr "Abre um navegador e remete-o para o índice deste manual."

#: ../../reference_manual/main_menu/help_menu.rst:20
msgid "Report Bug"
msgstr "Comunicar um Erro"

#: ../../reference_manual/main_menu/help_menu.rst:21
msgid "Sends you to the bugtracker."
msgstr "Envia-o para o sistema de gestão de erros."

#: ../../reference_manual/main_menu/help_menu.rst:22
msgid "Show system information for bug reports."
msgstr "Mostra informações do sistema para os relatórios de erros."

#: ../../reference_manual/main_menu/help_menu.rst:23
msgid ""
"This is a selection of all the difficult to figure out technical information "
"of your computer. This includes things like, which version of Krita you "
"have, which version your operating system is, and most prudently, what kind "
"of OpenGL functionality your computer is able to provide. The latter varies "
"a lot between computers and due that it is one of the most difficult things "
"to debug. Providing such information can help us figure out what is causing "
"a bug."
msgstr ""
"Esta é uma selecção de todos os dados técnicos que são difíceis de descobrir "
"acerca do seu computador. Isto inclui algumas coisas como a versão do Krita "
"que tem, qual a versão do seu sistema operativo e, com mais cuidado, o tipo "
"de funcionalidade do OpenGL que o seu computador consegue oferecer. A última "
"informação varia bastante entre computadores e, por causa disso, é uma das "
"coisas mais difíceis de diagnosticar. Ao fornecer-nos esta informação, poder-"
"nos-á ajudar o que possa estar a provocar um dado erro."

#: ../../reference_manual/main_menu/help_menu.rst:24
msgid "About Krita"
msgstr "Acerca do Krita"

#: ../../reference_manual/main_menu/help_menu.rst:25
msgid "Shows you the credits."
msgstr "Apresenta-lhe os créditos."

#: ../../reference_manual/main_menu/help_menu.rst:27
msgid "Tells you about the KDE community that Krita is part of."
msgstr ""
"Faz-lhe uma apresentação da comunidade do KDE, da qual o Krita faz parte."
