# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-09 03:41+0200\n"
"PO-Revision-Date: 2019-05-25 12:19+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Palette en guilabel image Kritamouseleft kbd images\n"
"X-POFile-SpellExtra: alt gpl icons RY RIFF ref sbz Scribus psp filekpl\n"
"X-POFile-SpellExtra: Photoshop Krita Swatchbooker aco riff act PaintShop\n"
"X-POFile-SpellExtra: docker mouseleft Kritamouseright dockers mouseright\n"
"X-POFile-IgnoreConsistency: Name\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../reference_manual/dockers/palette_docker.rst:1
msgid "Overview of the palette docker."
msgstr "Introdução à área da paleta."

#: ../../reference_manual/dockers/palette_docker.rst:12
#: ../../reference_manual/dockers/palette_docker.rst:74
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/dockers/palette_docker.rst:12
msgid "Palettes"
msgstr "Paletas"

#: ../../reference_manual/dockers/palette_docker.rst:12
msgid "Swatches"
msgstr "Amostras de Cores"

#: ../../reference_manual/dockers/palette_docker.rst:17
msgid "Palette Docker"
msgstr "Área da Paleta"

#: ../../reference_manual/dockers/palette_docker.rst:19
msgid ""
"The palette docker displays various color swatches for quick use. It also "
"supports editing palettes and organizing colors into groups, as well as "
"arbitrary positioning of swatches."
msgstr ""
"A área da paleta mostra diversas opções de cores para um uso rápido. Também "
"suporta a edição de paletas e a organização das cores por grupos, assim como "
"posicionar as amostras de forma arbitrária."

#: ../../reference_manual/dockers/palette_docker.rst:23
msgid ""
"The palette docker was overhauled in 4.2, allowing for grid ordering, "
"storing palette in the document and more."
msgstr ""
"A área da paleta foi remodelada no 4.2, permitindo a ordenação da grelha, a "
"gravação da paleta no documento, entre outras coisas."

#: ../../reference_manual/dockers/palette_docker.rst:26
msgid ".. image:: images/dockers/Palette-docker.png"
msgstr ".. image:: images/dockers/Palette-docker.png"

#: ../../reference_manual/dockers/palette_docker.rst:27
msgid ""
"You can choose from various default palettes or you can add your own colors "
"to the palette."
msgstr ""
"Poderá escolher entre várias paletas ou adicionar as suas próprias cores à "
"paleta."

#: ../../reference_manual/dockers/palette_docker.rst:29
msgid ""
"To choose from the default palettes click on the icon in the bottom left "
"corner of the docker, it will show a list of pre-loaded color palettes. You "
"can click on one and to load it into the docker, or click on import "
"resources to load your own color palette from a file. Creating a new palette "
"can be done by pressing the :guilabel:`+`. Fill out the :guilabel:`name` "
"input, pressing :guilabel:`Save` and Krita will select your new palette for "
"you."
msgstr ""
"Para escolher entre as várias paletas, carregue no ícone no canto inferior "
"esquerdo da área acoplável; o mesmo irá mostrar uma lista de paletas de "
"cores pré-carregadas. Poderá carregar numa para a carregar para a área "
"acoplável, ou então carregar em Importar os Recursos (ícone da pasta) para "
"carregar a sua própria paleta de cores a partir de um ficheiro. A criação de "
"uma paleta poderá ser obtida com o botão :guilabel:`+`. Preencha o campo :"
"guilabel:`nome`, carregando em :guilabel:`Gravar` para que o Krita "
"seleccione a sua nova paleta por si."

#: ../../reference_manual/dockers/palette_docker.rst:32
msgid ""
"Since 4.2 Krita's color palettes are not just a list of colors to store, but "
"also a grid to organize them on. That's why you will get a grid with "
"'transparency checkers', indicating that there is no entry. To add an entry, "
"just click a swatch and a new entry will be added with a default name and "
"the current foreground color."
msgstr ""
"Desde o 4.2, as paletas de cores do Krita já não são apenas uma lista de "
"cores a guardar, mas também uma grelha onde as poderá organizar. É por isso "
"que irá obter uma grelha com o 'xadrez de transparência, o que indica que "
"não existe nenhum elemento. Para adicionar um elemento, basta carregar numa "
"amostra para adicionar um novo elemento com um nome predefinido e a cor "
"principal actual."

#: ../../reference_manual/dockers/palette_docker.rst:34
msgid "Selecting colors is done by |mouseleft| on a swatch."
msgstr ""
"A selecção das cores é feita com o |mouseleft| sobre uma dada área colorida."

#: ../../reference_manual/dockers/palette_docker.rst:35
msgid ""
"Pressing the delete icon will remove the selected swatch or group. When "
"removing a group, Krita will always ask whether you'd like to keep the "
"swatches. If so, the group will be merged with the default group."
msgstr ""
"Se carregar no ícone para apagar, irá remover a amostra de cor ou grupo "
"seleccionados. Ao remover o grupo, o Krita irá sempre perguntar se deseja "
"manter as cores à mesma. Se for o caso, as mesmas serão adicionadas ao grupo "
"predefinido acima."

#: ../../reference_manual/dockers/palette_docker.rst:36
msgid ""
"Double |mouseleft| a swatch will call up the edit window where you can "
"change the color, the name, the id and whether it's a spot color. On a group "
"this will allow you to set the group name."
msgstr ""
"Faça duplo-click com |mouseleft| sobre uma cor para invocar a janela de "
"edição, onde poderá alterar a mesma, o seu nome, o ID e se é uma cor "
"pontual. Num grupo, isto permitir-lhe-á definir o nome do grupo."

#: ../../reference_manual/dockers/palette_docker.rst:37
msgid ""
"|mouseleft| drag will allow you to drag and drop swatches and groups to "
"order them."
msgstr ""
"Se arrastar com o |mouseleft|, poderá arrastar e largar as cores e os grupos "
"para os ordenar."

#: ../../reference_manual/dockers/palette_docker.rst:38
msgid ""
"|mouseright| on a swatch will give you a context menu with modify and delete "
"options."
msgstr ""
"Use o |mouseright| sobre uma amostra de cor para obter um menu de contexto "
"com opções para modificar e remover."

#: ../../reference_manual/dockers/palette_docker.rst:39
msgid "Pressing the :guilabel:`+` icon will allow you to add a new swatch."
msgstr ""
"Se carregar no ícone :guilabel:`+` poderá adicionar uma nova área para cores."

#: ../../reference_manual/dockers/palette_docker.rst:40
msgid ""
"The drop down contains all the entries, id numbers and names. When a color "
"is a spot color the thumbnail is circular. You can use the dropdown to "
"search on color name or id."
msgstr ""
"A lista contém todos os elementos, números de ID's e nomes. Quando uma cor é "
"pontual, a miniatura é circular. Poderá usar a lista para procurar pelo nome "
"ou ID da cor."

#: ../../reference_manual/dockers/palette_docker.rst:43
msgid ""
"Pressing the Folder icon will allow you to modify the palette. Here you can "
"add more columns, modify the default group's rows, or add more groups and "
"modify their rows."
msgstr ""
"Se carregar no ícone da Pasta, poderá modificar a paleta. Aqui poderá "
"adicionar mais colunas, modificar as linhas do grupo predefinido ou "
"adicionar mais grupos e modificar as suas linhas."

#: ../../reference_manual/dockers/palette_docker.rst:45
msgid "Palette Name"
msgstr "Nome da Paleta"

#: ../../reference_manual/dockers/palette_docker.rst:46
msgid ""
"Modify the palette name. This is the proper name for the palette as shown in "
"the palette chooser dropdown."
msgstr ""
"Modifica o nome da paleta. Este é o nome adequado da paleta, tal como "
"aparece na lista de selectores de paletas."

#: ../../reference_manual/dockers/palette_docker.rst:47
msgid "File name"
msgstr "Nome do ficheiro"

#: ../../reference_manual/dockers/palette_docker.rst:48
msgid ""
"This is the file name of the palette, which should be file system friendly. "
"(Avoid quotation marks, for example)."
msgstr ""
"Este é o nome do ficheiro da paleta, o qual deverá ser amigável para o "
"sistema de ficheiros (evite pontos de interrogação, por exemplo)."

#: ../../reference_manual/dockers/palette_docker.rst:49
msgid "Column Count"
msgstr "Número de Colunas"

#: ../../reference_manual/dockers/palette_docker.rst:50
msgid ""
"The amount of columns in this palette. This counts for all entries. If you "
"accidentally make it smaller than the amount of entries that take up "
"columns, you can still make it bigger until the next restart of Krita."
msgstr ""
"A quantidade de cores nesta paleta. Isto conta com todos os elementos. Se "
"por acaso torná-la mais pequena que a quantidade de elementos que acompanham "
"as colunas, poderá a mesma torná-la maior até que reinicie o Krita da "
"próxima vez."

#: ../../reference_manual/dockers/palette_docker.rst:52
msgid "Whether to store said palette in the document or resource folder."
msgstr "Se deve guardar a dita paleta no documento ou na pasta do recurso."

#: ../../reference_manual/dockers/palette_docker.rst:54
msgid "Resource Folder"
msgstr "Pasta do Recurso"

#: ../../reference_manual/dockers/palette_docker.rst:55
msgid "The default, the palette will be stored in the resource folder."
msgstr "A opção por omissão - a paleta será guardada na pasta de recursos."

#: ../../reference_manual/dockers/palette_docker.rst:57
msgid "Where is the palette stored:"
msgstr "Onde é guardada a paleta:"

#: ../../reference_manual/dockers/palette_docker.rst:57
msgid "Document"
msgstr "Documento"

#: ../../reference_manual/dockers/palette_docker.rst:57
msgid ""
"The palette will be removed from the resource folder and stored in the "
"document upon save. It will be loaded into the resources upon loading the "
"document."
msgstr ""
"A paleta será removida da pasta de recursos e guardada no documento após a "
"gravação. Será carregada nos recursos após carregar o documento."

#: ../../reference_manual/dockers/palette_docker.rst:59
msgid "Add group"
msgstr "Adicionar um grupo"

#: ../../reference_manual/dockers/palette_docker.rst:60
msgid ""
"Add a new group. On clicking you will be asked for a name and a set of rows."
msgstr ""
"Adiciona um novo grupo. Ao carregar na opção, ser-lhe-á pedido um nome e um "
"conjunto de linhas."

#: ../../reference_manual/dockers/palette_docker.rst:62
msgid ""
"Here you can configure the groups. The dropdown has a selection of groups. "
"The default group is at top."
msgstr ""
"Aqui poderá configurar os grupos. A lista tem uma selecção de grupos. O "
"grupo predefinido está no topo."

#: ../../reference_manual/dockers/palette_docker.rst:64
msgid "Row Count"
msgstr "Número de Linhas"

#: ../../reference_manual/dockers/palette_docker.rst:65
msgid ""
"The amount of rows in the group. If you want to add more colors to a group "
"and there's no empty areas to click on anymore, increase the row count."
msgstr ""
"A quantidade de linhas no grupo. Se quiser adicionar mais cores a um grupo e "
"não existirem mais espaço em branco onde carregar, aumente o número de "
"linhas."

#: ../../reference_manual/dockers/palette_docker.rst:66
msgid "Rename Group"
msgstr "Mudar o Nome do Grupo"

#: ../../reference_manual/dockers/palette_docker.rst:67
msgid "Rename the group."
msgstr "Muda o nome do grupo."

#: ../../reference_manual/dockers/palette_docker.rst:69
msgid ""
"Delete the group. It will ask whether you want to keep the colors. If so, it "
"will merge the group's contents with the default group."
msgstr ""
"Apaga o grupo. Isso irá perguntar se deseja manter as cores. Se for o caso, "
"irá reunir o conteúdo do grupo com o grupo predefinido."

#: ../../reference_manual/dockers/palette_docker.rst:70
msgid "Group Settings"
msgstr "Configuração do Grupo"

#: ../../reference_manual/dockers/palette_docker.rst:70
msgid "Delete Group"
msgstr "Remover o Grupo"

#: ../../reference_manual/dockers/palette_docker.rst:72
msgid "The edit and new color dialogs ask for the following:"
msgstr ""
"As janelas para edição e adição de uma nova cor pedem os seguintes dados:"

#: ../../reference_manual/dockers/palette_docker.rst:75
msgid "The color of the swatch."
msgstr "A cor da área de cor."

#: ../../reference_manual/dockers/palette_docker.rst:76
msgid "Name"
msgstr "Nome"

#: ../../reference_manual/dockers/palette_docker.rst:77
msgid "The Name of the color in a human readable format."
msgstr "O nome da cor num formato legível."

#: ../../reference_manual/dockers/palette_docker.rst:78
msgid "ID"
msgstr "ID"

#: ../../reference_manual/dockers/palette_docker.rst:79
msgid ""
"The ID is a number that can be used to index colors. Where Name can be "
"something like \"Pastel Peach\", ID will probably be something like "
"\"RY75\". Both names and ids can be used to search the color in the color "
"entry dropdown at the bottom of the palette."
msgstr ""
"O ID é um número que poderá ser usado para indexar as cores. Enquanto o Nome "
"poderá ser \"Pêssego Pastel\", o ID será algo do tipo \"RY75\". Tanto o nome "
"como o ID podem ser usados na pesquisa da cor na lista de elementos de cores "
"no fundo da paleta."

#: ../../reference_manual/dockers/palette_docker.rst:81
msgid "Spot color"
msgstr "Cor pontual"

#: ../../reference_manual/dockers/palette_docker.rst:81
msgid ""
"Currently not used for anything within Krita itself, but spot colors are a "
"toggle to keep track of colors that represent a real world paint that a "
"printer can match. Keeping track of such colors is useful in a printing "
"workflow, and it can also be used with python to recognize spot colors."
msgstr ""
"De momento não são usadas para nada, dentro do Krita em si, mas as cores "
"pontuais são uma opção para manter um registo das cores que representa uma "
"tinta verdadeira que uma impressora consiga produzir. Manter o registo "
"dessas cores é útil numa actividade de impressão, podendo ser usada com o "
"Python para reconhecer as cores pontuais."

#: ../../reference_manual/dockers/palette_docker.rst:83
msgid ""
"Krita's native palette format is since 4.0 :ref:`file_kpl`. It also supports "
"importing..."
msgstr ""
"O formato nativo da paleta do Krita é, desde o 4.0, o :ref:`file_kpl`. "
"Também suporta a importação de..."

#: ../../reference_manual/dockers/palette_docker.rst:85
msgid "Gimp Palettes (.gpl)"
msgstr "Paletas do Gimp (.gpl)"

#: ../../reference_manual/dockers/palette_docker.rst:86
msgid "Microsoft RIFF palette (.riff)"
msgstr "Paleta RIFF da Microsoft (.riff)"

#: ../../reference_manual/dockers/palette_docker.rst:87
msgid "Photoshop Binary Palettes (.act)"
msgstr "Paletas Binárias do Photoshop (.act)"

#: ../../reference_manual/dockers/palette_docker.rst:88
msgid "PaintShop Pro palettes (.psp)"
msgstr "Paletas do PaintShop Pro (.psp)"

#: ../../reference_manual/dockers/palette_docker.rst:89
msgid "Photoshop Swatches (.aco)"
msgstr "Paletas do Photoshop (.aco)"

#: ../../reference_manual/dockers/palette_docker.rst:90
msgid "Scribus XML (.xml)"
msgstr "Scribus XML (.xml)"

#: ../../reference_manual/dockers/palette_docker.rst:91
msgid "Swatchbooker (.sbz)."
msgstr "Swatchbooker (.sbz)."
