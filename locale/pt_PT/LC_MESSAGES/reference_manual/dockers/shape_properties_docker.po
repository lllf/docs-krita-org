# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 10:04+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en KritaShapePropertiesDocker image shapeedittool\n"
"X-POFile-SpellExtra: images ref dockers\n"

#: ../../reference_manual/dockers/shape_properties_docker.rst:1
msgid "Overview of the shape properties docker."
msgstr "Introdução à área de propriedades da forma."

#: ../../reference_manual/dockers/shape_properties_docker.rst:15
msgid "Shape Properties Docker"
msgstr "Área de Propriedades da Forma"

#: ../../reference_manual/dockers/shape_properties_docker.rst:18
msgid ".. image:: images/dockers/Krita_Shape_Properties_Docker.png"
msgstr ".. image:: images/dockers/Krita_Shape_Properties_Docker.png"

#: ../../reference_manual/dockers/shape_properties_docker.rst:21
msgid ""
"This docker is deprecated, and its functionality is folded into the :ref:"
"`shape_edit_tool`."
msgstr ""
"Esta área foi descontinuada e a sua funcionalidade foi incorporada na :ref:"
"`shape_edit_tool`."

#: ../../reference_manual/dockers/shape_properties_docker.rst:23
msgid ""
"This docker is only functional when selecting a rectangle or circle on a "
"vector layer. It allows you to change minor details, such as the rounding of "
"the corners of a rectangle, or the angle of the formula for the circle-shape."
msgstr ""
"Esta área só funciona quando seleccionar um rectângulo ou círculo numa "
"camada vectorial. Permite-lhe mudar alguns pequenos detalhes, como o "
"arredondamento dos cantos de um rectângulo ou o ângulo da fórmula para a "
"forma circular."
